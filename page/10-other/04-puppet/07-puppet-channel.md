# Puppet Channel

* [Channel beitreten](#channel-beitreten)
* [Channel verlassen](#channel-verlassen)
* [Channel Ausgabe](#channel-ausgabe)


#### Channel beitreten

```
SET _dbgChannel_ "MyDbgChannel"
...
>> /channel [_dbgChannel_]
```

* Definition des Channel-Namens in einer Variablen.
* Beitritt des Channels unter Nutzung des zuvor definierten Channel-Namens.



#### Channel verlassen

```
SET _dbgChannel_ "MyDbgChannel"
...
>> /gtell [_dbgChannel_] /exit
```

* Definition des Channel-Namens in einer Variablen.
* Verlassen des Channels unter Nutzung des zuvor definierten Channel-Namens.



#### Channel Ausgabe

```
SET _dbgChannel_ "MyDbgChannel"
SET text "Langer Text ... tralalalala"
...
>> /gtell [_dbgChannel_] [text]
>> /gtell [_dbgChannel_] Noch mehr tralalalala
```
