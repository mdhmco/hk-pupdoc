# Puppeteinstellungen

Unterteilung der Puppeteinstellungen.

* [Einstellungen für Puppettexte](#einstellungen-für-puppettexte)
* [Einstellungen für Puppetcode](#einstellungen-für-puppetcode)
* [Einstellungen für Puppetowning](#einstellungen-für-puppetowning)
* [Einstellungen für Puppetverhalten](#einstellungen-für-puppetverhalten)
* [Einstellungen für Puppetdeklaration](#einstellungen-für-puppetdeklaration)


### Einstellungen für Puppettexte

#### LOGIN

- Diese Einstellung legt den Text fest, der bei der Anmeldung ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " materialisiert sich aus milliarden winziger Teilchen."


#### LOGOUT

- Diese Einstellung legt den Text fest, der bei der Abmeldung ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " zerfaellt zu Staub."


#### APPEAR

- Diese Einstellung legt den Text fest, der beim Betreten eines Raumes ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " verdichtet sich aus einer Wolke glitzernder Partikel."


#### DISAPPEAR

- Diese Einstellung legt den Text fest, der beim Verlassen eines Raumes ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " zerspringt in millionen funkelnder Partikel."


#### INFO

- Es wird die Beschreibung des Puppets festgelegt, die beim `\info`-Kommando angezeigt wird.
- *Default*: "Ein CommandPuppet."



### Einstellungen für Puppetcode

#### NATION

- Entspricht in der BrettspielWelt.prop in etwa `NATION = <lang>` oder einem frühzeitigen `>> /language <lang>` z.B. in der ACTION "start".
- *Default*: `bsw`

> Hinweis: Die Sprache "bsw" ist zu empfehlen, da diese nicht durch unbedarfte Übersetzer kaputt gemacht werden kann.

<!-- {% hint style='tip' %}
Die Sprache "bsw" ist zu empfehlen, da diese nicht durch unbedarfte Übersetzer kaputt gemacht werden kann.
{% endhint %} -->

<!-- <p class="box tip">
  Die Sprache "bsw" ist zu empfehlen, da diese nicht durch unbedarfte Übersetzer kaputt gemacht werden kann.
</p> -->

#### CITYCHAT

- Legt fest, ob das Puppet den Citychat des Starters benutzt. Bei Autostart-Puppets gilt derjenige als Starter, der das Puppet letztmalig manuell gestartet hat.
- `CITYCHAT:YES` bewirkt automatisch `NEWOWNER:NO`.
- Wird ein Puppet von einem anderen Puppet gestartet, so kann es Probleme mit dem Stadt-Chat geben
- *Default*: `NO`


#### SAVE

- Der SAVE-Befehl macht die SAVE-Variablen persistent, d.h. wenn das Puppet wieder geladen wird, sind diese Variablen mit den gespeicherten Werte belegt.

  - Die Angabe "phrase" sollte weltweit eindeutig sein (Sonderzeichen werden automatisch durch "_" ersetzt).
  - "*" in "phrase" wird durch den Namen ersetzt, mit dem das Puppet in der BSW gestartet wird (dies erhöht die Chance auf Eindeutigkeit).
  - Bei gleichem Namen wird dieselbe Datenbasis benutzt. Dies kann in seltenen Fällen Sinn machen (allerdings sollten zwei Puppets mit denselben Daten nicht gleichzeitig gestartet sein).
- *Default*: keiner

> Hinweis: Wird `SAVE:` nicht angegeben wird, dann werden keine SAVE-Variablen gespeichert.

<!-- {% hint style='tip' %}
Wird `SAVE:` nicht angegeben wird, dann werden keine SAVE-Variablen gespeichert.
{% endhint %} -->



### Einstellungen für Puppetowning

#### OWN

- Stellt ein, ob man das Puppet nach dem Start vom Starter geowned wird.
- *Default*: `YES`


#### NEWOWNER

- Legt fest, ob das Puppet von anderen Benutzern geowned werden darf.
- Wird automatisch auf `NO` gestellt, wenn `CITYCHAT:YES` gesetzt ist.
- *Default*: `YES`


#### TWINDOW

- Das, was ein Puppet zu hören bekommt, wird nach `/own PUPPET` durch `TWINDOW:YES` in einem extra Fenster dargestellt. Ansonsten erscheint dies im normalen Chat.
- *Default*: `NO`



### Einstellungen für Puppetverhalten

#### CASESENSITIV

- Gibt an, ob das Puppet Groß-/Kleinschreibung bei den Events `CHAT`, `KEYWORD` und `MATCH` unterscheidet.
- Die Einstellung `CASESENSITIV:NO` wird auch bei `==, !=, IN` (nur `EVAL`!), `STARTSWITH`, `ENDSWITH` und `INLIST` beachtet.
- *Default*: `NO`


#### OVERFLOW

- Es wird festgelegt, was geschehen soll, wenn das Puppet zu viele
 Ressourcen (Speicher, ..) verbraucht:
  - `ERROR` = Es wird eine ERROR-Action aufgerufen (siehe Kommando [ERROR-Ereignis &raquo; WHEN ERROR](../02-befehle/04-ereignisse/11-error)).
  - `HARAKIRI` = Das Puppet beendet sich.
  - `IGNORE` = Das Puppet ignoriert den Fehler.
- *Default*: `HARAKIRI`


#### ZEROINTVARS

- Wenn `YES` angegeben wird, werden leere Variablen als "0" aufgefasst.
- *Default*: `NO`

> Hinweis: Es gilt [undefined] >= 0 und [undefined] <= 0 aber nicht [undefined] == 0.

<!-- {% hint style='tip' %}
Es gilt [undefined] >= 0 und [undefined] <= 0 aber nicht [undefined] == 0.
{% endhint %} -->


#### DEBUG

- Hiermit wird der DEBUG-Modus angeschaltet. Erlaubt ist jede Kombination der Einstellungen: `ACTION`, `EVENT`, `VAR(IABLES)`, `(SINGLE)STEP`, `LOCAL`, `SCOPE` und `TOOL`
- Ausserdem `ON`, `OFF`, `ALL` und `NONE`.
- *Default*: keiner

> Siehe: PuppetTool und Kapitel [Debugging](../10-other/02-debug/00-page.md)



##### @`<command>`
- Aufruf der festgelegten Action als @-Befehl.
- *Default*: keiner

> Siehe: Kapitel [@-Befehle](../02-grundlagen/04-addbefehle.md#addbefehle)



### Einstellungen für Puppetdeklaration

#### CHARSET

- Der Zeichensatz des Quelltextes wird festgelegt.
  - `ISO-8859-1`
  - `UTF-8`
  - `Shift_JIS`
- *Default*: `ISO-8859-1`
