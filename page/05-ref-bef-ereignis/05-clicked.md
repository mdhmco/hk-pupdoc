# CLICKED-Ereignis

* [WHEN CLICKED](#when-clicked)
* [IGNORE CLICKED](#ignore-clicked)


#### WHEN CLICKED

```
WHEN CLICKED [erweiterte WHEN Bedingungen] DO <action>
```

...TO BE SPECIFIED...


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Sprecher des Textes
`CHAT` = Eingegebener Text
`ITEM` = ???
`TYPE` = Art des Chats
`ROOM` = Aufenthaltsort des Sprechers
`ROOMNAME` = Angabe zum Aufenthaltsort des Sprechers
{%- endcodetabs %}



#### IGNORE CLICKED

```
IGNORE CLICKED [erweiterte WHEN Bedingungen]
```

...TO BE SPECIFIED...
