# NEWROOM-Ereignis

* [WHEN NEWROOM](#when-newroom)
* [IGNORE NEWROOM](#ignore-keywonewroomrd)


#### WHEN NEWROOM

```
WHEN NEWROOM DO <action>
```

- Wenn das Puppet den Raum wechselt, wird die Action `<action>`  ausgeführt.


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
* `ROOM` = ID des neuen Raumes
{%- endcodetabs %}



#### IGNORE NEWROOM

```
IGNORE NEWROOM
```

- Raumwechsel werden nicht mehr beachtet.
