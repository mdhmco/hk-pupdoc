# KICKED-Ereignis

* [WHEN KICKED](#when-kicked)
* [IGNORE KICKED](#ignore-kicked)


#### WHEN KICKED

```
WHEN KICKED DO <action>
```

- Wenn jemand das Puppet kickt, hat das Puppet noch ca. 2 Sekunden Zeit ein  paar (Sleepy-)Befehle auszuführen, bevor es beendet wird. 
- Dafür wird sofort die Action `<action>` ausgeführt.
- Stehen noch andere Befehle an, so wird die Abarbeitung dieser zurückgestellt.


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Person, die das Puppet gekickt hat
{%- endcodetabs %}


{% hint style='warning' %}
Sleepy-Befehle sind `>>`, `SLEEP`, `MASTERRESET` und `HARAKIRI`. Ausserdem ist ca. jeder 100ste Befehl automatisch "sleepy".
{% endhint %}  



#### IGNORE KICKED

```
IGNORE KICKED
```

- Nichts passiert, wenn das Puppet gekickt wird.
