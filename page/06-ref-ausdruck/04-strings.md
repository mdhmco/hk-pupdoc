# Ausdrücke für Strings

* [ISEMPTY](#isempty)
* [= und != Operator](#und--operator)
* [MATCHES](#matches)
* [STARTSWITH / ENDSWITH](#startswith--endswith)
* [LENGTH](#length)
* [CHAROF](#charof)
* [FIRSTCHAROF / LASTCHAROF](#firstcharof--lastcharof)
* [WITHOUTFIRSTCHAR / WITHOUTLASTCHAR](#withoutfirstchar--withoutlastchar)
* [LOWERCASE / UPPERCASE](#lowercase--uppercase)


#### ISEMPTY

```
ISEMPTY <string>
```

Liefert `TRUE`, falls `<string>` der leere String ("") ist, ansonsten `FALSE`.



#### == und != Operator

```
<string> == <string>
<string> != <string>
```

Liefert `TRUE`, falls beide Strings gleich / verschieden sind, ansonsten `FALSE`.



#### MATCHES

```
<string1> MATCHES <string2>
```

Liefert `TRUE`, wenn `<string1>` die Form `<string2>` hat (siehe [MATCH-Ereignis &raquo; WHEN MATCH](../05-ref-bef-ereignis/04-match.md)). Dabei kann `<string2>` Wildcards enthalten, die in `SUBST[n]` - Variablen umgesetzt werden.


{% codetabs name="Beispiel", type="puppet" -%}
WHEN MATCH "-- SpielErgebnis *" IN GTELL "..." FROM "--" DO result_work
# ...
ACTION result_work
  # ...
  # --- Text zerlegen
  IF NOT [SUBST1] MATCHES "* \[C*-*\]: *"
    RETURN
  # --- Daten übernehmen
  LOCAL resGame = [SUBST1]
  LOCAL resCityNr = [SUBST2]
  LOCAL resRoomNr = [SUBST3]
  LOCAL resString = [SUBST4]
  # ...
END
{%- endcodetabs %}



#### STARTSWITH / ENDSWITH

```
<string> STARTSWITH <string>
<string> ENDSWITH <string>
```

Liefert `TRUE`, falls der erste String mit dem zweiten String beginnt / endet, ansonsten `FALSE`.



#### LENGTH

```
LENGTH <string>
```

Liefert die Länge von `<string>` (=Anzahl der Zeichen).



#### CHAROF

```
<n> CHAROF <string>
```

Liefert das `<n>`-te Zeichen von `<string>`.



#### FIRSTCHAROF / LASTCHAROF

```
FIRSTCHAROF <string>
LASTCHAROF <string>
```

Liefert das erste / letzte Zeichen von `<string>`.



#### WITHOUTFIRSTCHAR / WITHOUTLASTCHAR

```
WITHOUTFIRSTCHAR <string>
WITHOUTLASTCHAR <string>
```

Liefert alle Zeichen von `<string>`, ausser dem ersten / letzten.



#### LOWERCASE / UPPERCASE

```
LOWERCASE <string>
UPPERCASE <string>
```

Liefert einen String, bei dem alle Zeichen von `<string>` klein / gross geschrieben sind.

{% hint style='tip' %}
Das deutsche "ß" wird bei `UPPERCASE` zu "SS". 
{% endhint %}
