# Historie des Dokuments

#### Generierung via HonKit

* V1.60.x (12/2022 - 12/2022)
  - Umstellung auf **HonKit**
  - Ergänzt um ein paar individuelle CSS Klassen
  - Geringfügige inhaltliche Überarbeitungen
  - Ergänzungen
    - Vereinzelte Erweiterungen aus 2022
    - Kapitel "Puppet Channel": Neue Beschreibung
    - Kapitel "Puppet Resultchannel": Neue Beschreibung, incl. 1024+2048
    - Kapitel "Befehle / Information": GETROOMINFO - CITYHOUSETYPE CITYHOUSETYPENAME


#### Generierung via VuePress

* V1.51.x (09/2021 - 10/2021)
  - Geringfügige Überarbeitungen
  - Kleinere Korrekturen
  - Weitere Anpassungen der Struktur
    - Randeffekt: Zergliederung in übersichtlichere Teiltexte
  - Ergänzungen
    - Kapitel "BSW Kommandos": Zwei Einschränkungen bezüglich Autostartpuppets
    - Wiederaufnahme von Kapiteln, die scheinbar im Laufe der Zeit unabsichtlich abhanden gekommen waren.
    - Kapitel "Weitere Infos" ➞ "Puppet Rahmenbedingungen"

* V1.50.0 (08/2021 - 09/2021)
  - Umstellung auf **VuePress**
    - Anpassung der Struktur
    - Löschung von Sphinx-spezifischen Angaben
  - GitLab Projekt


#### Generierung via Sphinx

* V1.30.x und V1.40.x (09/2018 - 01/2019)
  - Geringfügige Überarbeitungen
  - Festlegung der Lizenzbedingungen
  - Umstellung auf **Sphinx**


#### Generierung via Pandoc

* V1.20 (07/2018)
  - Rück-Umstellung auf **Pandoc**


#### Generierung via DokuWiki

* V1.11 (09/2017)
  - Ergänzung zu “WHEN TIME”
* V1.10 (01/2017)
  - Spielersetzung “EinfachGenial” durch “Imhothep”.
* V1.01 (12/2016) - V1.03 (12/2016)
  - Geringfügige Überarbeitungen
  - Umstellung auf **DokuWiki**


#### Generierung via div. Tools

* Dieser Teil der Historie wurde gelöscht!
* Erste Version des Dokuments in **2006**
