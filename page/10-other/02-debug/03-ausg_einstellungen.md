# Ausgaben der DEBUG-Einstellungen

* [Debug ACTION](#debug-action)
* [Debug EVENT](#debug-event)
* [Debug VARIABLES](#debug-variables)
* [Debug LOCAL](#debug-local)
* [Debug SINGLESTEP](#debug-singlestep)


#### Debug ACTION

Der Aufruf einer Action und die Ausführung der `CASE`-Zweige eines `SWITCH` werden angezeigt.

```
A <action>
A <action> (sofort)
SWC ...
SWA ...
```

* Kennung =
  * `A` = Action
  * `SWC` = `CASE`
  * `SWA` = `CASEALL`
* `<action>` = Name der Action
* (sofort) = Action wird vorrangig ausgeführt (siehe etwa `WHEN KICKED`, aber auch ``DO``)



#### Debug EVENT

Jedes Mal, wenn ein Event auftritt (unabhängig davon, ob der Event auch mit einem `WHEN`-Befehl abgefragt wird) wird ein

```
E: <typ> <weitere Infos>
```

ausgegeben. `<typ>` gibt an, was für ein Event es ist, und abhängig davon, gibt `<weitere Infos>` weitere Informationen dazu an.

> Siehe: Beschreibung des [EVENTDUMP-Befehls](../../04-ref-befehl/12-debugging.md#eventdump-befehl)



#### Debug VARIABLES

Gibt bei jeder Änderung einer Variablen aus

```
V <name>: <alterwert> => <neuerwert>
```



#### Debug LOCAL

Liefert Informationen über Anfang und Ende lokaler Variablen. Ausgabe zum Beispiel so: 

```
L + Label 337 in Zeile 564 angelegt: (LOCALLABEL) <= Label 0: (LOCAL 2. mit Parameter)
L - Label0 337 in Zeile 567 entfernt: (LOCALLABEL)
```

* Zunächst wird die lokale Variable "Label" in Zeile 564 angelegt (ID=337) und mit "LOCALLABEL" initialisiert, wodurch die globale Variable (immer ID=0) mit dem Wert "LOCAL 2. mit Parameter" erst einmal verborgen wird.
* Später, in Zeile 567, wird dann der alte Wert hervorgekramt...


Es sind auch Rekursionen möglich, die in jeder Stufe eine neue lokale Variable erzeugen.


{% hint style='tip' %}
<code>@@debug LOCAL</code> kann genutzt werden, um die von den Befehlen gesetzten Variablen zu ermitteln (wenn die Befehlsliste unvollständig ist).
{% endhint %}



#### Debug SINGLESTEP

{% hint style='danger' %}
<code>SINGLESTEP</code> kann sehr viel Ausgabe erzeugen und die Ausführung des Programms drastisch verlangsamen. Es ist deshalb ratsam, nur den interessanten Teil des Codes durch das Kommando <code>DEBUG ON/OFF</code> debuggen zu lassen.
{% endhint %}

<!-- <p class="box alert">
  <code>SINGLESTEP</code> kann sehr viel Ausgabe erzeugen und die Ausführung des Programms drastisch verlangsamen. Es ist deshalb ratsam, nur den interessanten Teil des Codes durch das Kommando <code>DEBUG ON/OFF</code> debuggen zu lassen.
</p> -->

<!-- <span class="mdi mdi-alert-octagon" style="font-size:18px; color:red"></span>
ACHTUNG -->

