# Sprachelemente

## String

Strings werden durch Gänsefüßchen (") umschlossen. Soll innerhalb eines Strings ein Gänsefüßchen stehen, so ist dieses doppelt zu schreiben.

Enthält der String keine Leerzeichen, so können die Gänsefüßchen auch entfallen. Ausnahme sind [Enggeschriebene Formeln](../06-ref-ausdruck/09-auswertung.md#enggeschriebene-formeln).


{% codetabs name="Beispiel", type="puppet" -%}
SET Name1 "Hans"          # korrekt
SET Name2 Hans            # auch ohne "" korrekt
SET Name3 "Hans-Jakob"    # ohne "" wäre es falsch, da enggeschriebene Formel
SET Name4 "Hans Meier"    # ohne "" wäre es falsch, da Leerzeichen enthalten
# Zeichenkette >>Dieser Text enthält Gänsef"ü"ßchen.<<
SET text "Dieser Text enthält Gänsef""ü""ßchen."
{%- endcodetabs %}



## Liste

Listen sind nichts anderes als Strings, bei denen die Leerzeichen die Elemente der Liste voneinander trennen.

{% codetabs name="Beispiel", type="puppet" -%}
SET liste1 "Entry1 Entry2 Entry3"
SET liste2 "[liste1] Entry4 Entry5"
UNSET liste3
ADDLIST liste3 [liste1]
ADDLIST liste3 "Entry4 Entry5"
{%- endcodetabs %}


{% hint style='tip' %}
Die 1-elementige Liste mit einem Leerstring ("") ist nicht zu verwechseln mit der leeren Liste "".
{% endhint %}

<!-- <p class="box tip">
  Die 1-elementige Liste mit einem Leerstring ("") ist nicht zu verwechseln mit der leeren Liste "".
  < ! - - <span class="mdi mdi-message-arrow-right-outline"></span> - - >
</p> -->

<!-- > <span class="mdi mdi-message-arrow-right-outline"></span> Die 1-elementige Liste mit einem Leerstring ("") ist nicht zu verwechseln mit der leeren Liste "". -->

<!-- > <span class="mdi mdi-lightbulb-on-outline tip" aria-hidden></span> Die 1-elementige Liste mit einem Leerstring ("") ist nicht zu verwechseln mit der leeren Liste "". -->



## Kommentar

Kommt in einer Zeile ein Doppelkreuz (#) vor, so wird alles nach diesem Doppelkreuz ignoriert. Dies ist nicht der Fall, wenn das Doppelkreuz in einem String steht, also von Gänsefüßchen umrahmt ist.

Weiterhin werden Leer-Zeilen komplett ignoriert.



## Steuerzeichen

Befinden sich Steuerzeichen in der Eingabe, so werden diese durch einen Punkt (.) ersetzt. Eine Ausnahme bildet das Tab-Steuerzeichen, welches durch ein Leerzeichen ersetzt wird.

Leerzeichen und Steuerzeichen am Anfang und am Ende einer Zeile werden entfernt.



## Variable

Veränderbare Inhalte werden von Puppets in Variablen gehalten. Soll deren Inhalt verwendet werden, so wird der Name der Variablen in einem eckigen Klammerpaar angegeben.

Der Name einer Variablen darf nicht jedes beliebige Zeichen beinhalten. Auf der sicheren Seite ist man mit Buchstaben und Ziffern.

> Siehe: Kapitel [Variablenersetzung](../03-referenz/02-variablen.md#variablenersetzung)


{% codetabs name="Beispiel", type="puppet" -%}
# Definition einer Variablen
SET nickname "Lieschen"
# Verwendung des Inhaltes einer Variablen
>> /tell [nickname] huhuuuuuuuu
{%- endcodetabs %}



## Befehle und Ausdrücke

Es ist zu unterscheiden zwischen "Befehle" und "Ausdrücke".

* Befehle geben eine Aktion an, die auszuführen ist und stehen mit ihren Parametern in einer eigenen Zeile.
* Ausdrücke liefern ein Ergebnis, welches in weiteren Ausdrücken oder innerhalb eines Befehls verwendet werden kann. Diese stehen oft rechts von einem Gleichheitszeichen.

> Siehe: Referenz der [Befehle](../04-ref-befehl/00-readme.md) und [Ausdrücke](../06-ref-ausdruck/00-readme.md)
