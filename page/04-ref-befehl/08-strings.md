# Befehle für Strings

* [FIND-Befehl](#find-befehl)
* [REPLACE-Befehl](#replace-befehl)
* [SUBSTR-Befehl](#substr-befehl)


#### FIND-Befehl

```
FIND <varname> <zeichen>
```

* Sucht in der Variablen mit Namen `<varname>` alle Zeichen `<zeichen>`.


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
* `INDEXLEN` = Anzahl der Treffer
* `INDEX*` = Positionen der Treffer (\*=1,2,...)
{%- endcodetabs %}



#### REPLACE-Befehl

```
REPLACE <varname> <von> <nach>
```

* Ersetzt in der Variablen mit Namen `<varname>` alle Vorkommen `<von>` durch `<nach>` (ohne Rücksicht auf Listenformate).



#### SUBSTR-Befehl

```
SUBSTR <varname> <indexVon> <indexBis>
```

* Ersetzt den String mit Namen `<varname>` durch eine Teilmenge seiner selbst.
* Die Teilmenge wird bestimmt durch die Zeichen zwischen den Indexpositionen `<indexVon>` und `<indexBis>`.
* Wird `<indexBis>` weggelassen, so werden die Zeichen bis zum Ende genommen.
* Für `<indexVon>` gleich `<indexBis>` empfiehlt sich der Ausdruck [CHAROF](../04-ausdruck/04-strings.md#charof).
