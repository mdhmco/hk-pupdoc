# Puppets

Erläuterung zu Puppets.

* [Puppet Zeichensatz](./01-puppet-enc.md)
* [Puppet Registrierung](./02-puppet-reg.md)
* [Puppet Typen](./03-puppet-typen.md)
* [Puppet Anzeigen](./04-puppet-anzeigen.md)
* [Puppet Ausführung](./05-puppet-ausfuehrung.md)
* [Puppet Rahmenbedingungen](./06-puppet-rahmenbed.md)
* [Puppet Channel](./07-puppet-channel.md)
* [Puppet Resultchannel](./08-puppet-resultchannel.md)

