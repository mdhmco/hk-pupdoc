# Puppet Resultchannel

* [Erweiterte Result Optionen](#erweiterte-result-optionen)
* [Erweiterte Result Option - 1024](#erweiterte-result-option---1024)
* [Erweiterte Result Option - 2048](#erweiterte-result-option---2048)


#### Erweiterte Result Optionen

Um weitere Informationen zu einem Spiel-Result zu erhalten, kann ein Benutzermodus im jeweiligen **Result-Channel** gesetzt werden.

Der Befehl lautet:

```
/usermode result={Zahl}
```

Wobei die Zahl die Summe aus den Zahlen der möglichen, nachfolgenden Optionen ist.

* 1 = Kein Result anzeigen
  * Ist diese Option aktiv, wird kein Result ausgegeben. (Alle anderen Optionen werden ignoriert)
* 2 = Spielname
  * Der Name des Spiels.
* 4 = Spiel-ID
  * Die ID des Spiels.
* 8 = Raumnummer
  * Die Raumnummer.
* 16 = Mitspieler
  * Die Anzahl der Mitspieler.
* 32 = Spielergebnis
  * Das Spielergebnis ansich.
* 64 = Spieldauer
  * Die Dauer des Spiels.
* 128 = Spielende
  * Datum/Zeit im Format Jahr-Monat-Tag-Stunde-Minute-Sekunde-Zeitzone zum Zeitpunkt des Spielendes.
* 256 = Spielende als Timestamp
  * Datum/Zeit als Unix-Timestamp.
* 512 = Optionen
  * Die aktivierten Spieloptionen


{% codetabs name="Beispiel 1", type="txt" -%}
Gewünscht: Spiel-ID (4), Anzahl der Mitspieler (16) und Spieldauer (64)
Summe: 4 + 16 + 64 = 84

/usermode result=84

-- SpielErgebnis: 72 | 4 | 0:10:29

{%- language name="Beispiel 2", type="txt" -%}
Gewünscht: Alle Angaben
Summe: 2 + 4 + 8 + 16 + 32 + 64 + 128 + 256 = 1022

/usermode result=1022

-- SpielErgebnis: Dog | 99 | C83-16 | 4 | 3.,SpielerA,4; 1.,SpielerC,8; 3.,SpielerB,4; 1.,SpielerD,8 | 0:35:22 | 2013-02-20-08-54-34-CET | 1363766074 | RESULT:Hinkelstein

{%- language name="Beispiel 3", type="txt" -%}
Gewünscht: kein Result (1)
Summe: 1 = 1

/usermode result=1

** Keine Ausgabe **

{%- language name="Beispiel 4", type="txt" -%}
/usermode result=off

Reaktivierung der ursprünglichen Result-Variante

-- SpielErgebnis Clans [C83-24]: 2.,SpielerA,27; 1.,SpielerB,36
{%- endcodetabs %}

> Quelle: BSW Puppet Forum >> Erweiterte Result Option




#### Erweiterte Result Option - 1024

* 1024 = Raum-Instanz-Nummer
  * Das ist die Raum-Instanz-Nummer. Bei jedem Spielstart wird ja ne neue "Instanz" angelegt, damit pro Raum mehrere Spiele gehen. Also die Nummer hinter dem Punkt C123-45.1 .


{% codetabs name="Beispiel", type="txt" -%}
Gewünscht: ??? plus Raum-Instanz-Nummer
Summe: ... + 1024 = ???

/usermode result=???

-- SpielErgebnis: Geister | 33 | C1-21 | 2 | 1.,Silbermondauge,992; 2.,Goldsonnenmund,0 | 0:01:34 | 2022-12-17-19-00-33-CET | 1671300033 | result:Glubsch | 1 | 1;1
{%- endcodetabs %}

> Weitere Quellen: Mitteilung per Tell und/oder PN



#### Erweiterte Result Option - 2048

* 2048 = Liste der Stadtnummern
  * Das wird dann die Stadtzugehörigkeit der user hinten in der Form z.B. 1;83;210;0 ausgeben.


{% codetabs name="Beispiel", type="txt" -%}
Gewünscht: ??? plus Liste der Stadtnummern
Summe: ... + 2048 = ???

/usermode result=???

-- SpielErgebnis: ... ??? ...
{%- endcodetabs %}


{% hint style='tip' %}
Es gibt eine Registrierungspflicht im Client und Browser. Daher stellt sich bei der Option 2048 nicht die Frage nach einer Unterscheidung zwischen ARMfeldern und unreg. Usern.
{% endhint %}

> Weitere Quellen: Mitteilung per Tell und/oder PN
