# TIMER-Ereignis

* [WHEN TIMER](#when-timer)
* [IGNORE TIMER](#ignore-timer)


#### WHEN TIMER

```
WHEN TIMER <sek> DO <action>
```

- Alle `<sek>` Sekunden wird die Action `<action>` ausgeführt.


{% hint style='warning' %}
Das Puppet kann sich nur einen Timer merken.
{% endhint %}



#### IGNORE TIMER

```
IGNORE TIMER
```

- Der Timer wird abgeschaltet.
