# Anhang


#### Dokumentation der BSW

* [FAQ Puppets](http://www.brettspielwelt.de/Hilfe/FAQ/Puppets/) = FAQ - Häufig gestellte Fragen
* [Puppet-Werkzeug](http://www.brettspielwelt.de/Hilfe/Tools/Puppets/) = Anleitung zum Puppet-Werkzeug


#### Dokumentation aus anderen Quellen

* Mittlerweilen Fehlanzeige!


#### TEST HINTS

{% hint style='info' %}
Important INFO: this note needs to be highlighted
{% endhint %}

{% hint style='tip' %}
Important TIP: this note needs to be highlighted
{% endhint %}

{% hint style='danger' %}
Important DANGER: this note needs to be highlighted
{% endhint %}

{% hint style='warning' %}
Important WARNING: this note needs to be highlighted
{% endhint %}

{% hint style='link' %}
Important LINK: this note needs to be highlighted
{% endhint %}
