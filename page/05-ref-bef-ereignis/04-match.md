# MATCH-Ereignis

* [WHEN MATCH](#when-match)
* [IGNORE MATCH](#ignore-match)
* [IGNOREALL MATCH](#ignoreall-match)


#### WHEN MATCH

```
WHEN MATCH <match> [erweiterte WHEN Bedingungen] DO <action>
```

- Wenn das Puppet Eingabezeilen mitbekommt, auf die `<match>` passt, so wird Action `<action>` ausgeführt.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Sprecher des Textes
`CHAT` = Eingegebener Text
`SUBST1` = Substitution für erstes * in ``MATCH``
`SUBST2` = Substitution für zweites * in ``MATCH``
`SUBSTx` = Substitution für x-tes * in ``MATCH``
`SUBSTLEN` = Anzahl der Substitutionen 
`TYPE` = Art des Chats
`ROOM` = Aufenthaltsort des Sprechers
`ROOMNAME` = Angabe zum Aufenthaltsort des Sprechers
{%- endcodetabs %}


> Siehe: `TYPE`, `ROOM`und `ROOMNAME` im Kapitel [Variablen von Befehlen](../03-referenz/02-1-var_befehle.md)



#### IGNORE MATCH

```
IGNORE MATCH <match> [erweiterte WHEN Bedingungen]
```

- Der `<match>` wird ab sofort nicht mehr überprüft bzw. nicht mehr
  gesondert behandelt, falls noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.



#### IGNOREALL MATCH

```
IGNOREALL MATCH [erweiterte WHEN Bedingungen]
```

- Der `<match>` wird ab sofort nicht mehr überprüft bzw. nicht mehr gesondert behandelt, falls noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.
