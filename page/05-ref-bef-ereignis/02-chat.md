# CHAT-Ereignis

* [WHEN CHAT](#when-chat)
* [IGNORE CHAT](#ignore-chat)
* [IGNOREALL CHAT](#ignoreall-chat)


#### WHEN CHAT

```
WHEN CHAT [erweiterte WHEN Bedingungen] DO <action>
```

- Das Puppet reagiert auf die Ausgabe von Text, indem die angegebene Action aufgerufen wird.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Sprecher des Textes
`CHAT` = Eingegebener Text
`TYPE` = Art des Chats ()
`ROOM` = Aufenthaltsort des Sprechers
`ROOMNAME` = Angabe zum Aufenthaltsort des Sprechers
{%- endcodetabs %}


> Siehe: `TYPE`, `ROOM`und `ROOMNAME` im Kapitel [Variablen von Befehlen](../03-referenz/02-1-var_befehle.md)


{% codetabs name="Beispiel", type="puppet" -%}
ACTION eineAction
  # Registrierung diverser Actions
  #  - Reaktion auf Textausgaben von einer bestimmten Person
  WHEN CHAT FROM "Kugelschreiber" DO kuliAction
  #  - Reaktion auf Textausgaben von Personen, die in einer Liste stehen
  WHEN CHAT FROM "[Freunde]" DO freundeAction
  #  - Reaktion auf Textausgaben von Personen, zu denen sonst keinen Eintrag haben
  WHEN CHAT FROM "*" DO sonstAction
END
{%- endcodetabs %}



#### IGNORE CHAT

```
IGNORE CHAT [erweiterte WHEN Bedingungen]
```

- Das Puppet ignoriert ab sofort die Ausgabe von Text.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.


{% codetabs name="Beispiel", type="puppet" -%}
ACTION eineAction
  # De-Registrierung diverser Actions
  #  - Ignoriert Textausgaben einer bestimmten Person
  IGNORE CHAT FROM "Kugelschreiber"
  #  - Ignoriert Textausgaben der Personen, die in einer Liste stehen
  IGNORE CHAT FROM "[Freunde]"
  #  - Ignoriert alle Textausgaben
  IGNORE CHAT
END
{%- endcodetabs %}



#### IGNOREALL CHAT

```
IGNOREALL CHAT [erweiterte WHEN Bedingungen]
```

- Das Puppet ignoriert ab sofort die Ausgabe von beliebigem Text.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.
