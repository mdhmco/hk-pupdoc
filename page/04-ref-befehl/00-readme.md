# Referenz Befehle

Erläuterung der Befehle.

* [Befehle zur Puppetstruktur](./01-struktur.md)
* [Befehle zur Steuerung](./02-steuerung.md)
* [Befehle für Ereignisse](../05-ref-bef-ereignis/00-readme.md)
* [Befehle für Informationen](./05-information.md)
* [Befehle für Textausgabe](./06-textausgabe.md)
* [Befehle für Variablen](./07-variablen.md)
* [Befehle für Strings](./08-strings.md)
* [Befehle für Listen](./09-listen.md)
* [Befehle für Daten](./10-daten.md)
* [Befehle für Kommandoausführung](./11-cmdausf.md)
* [Befehle für Debugging](./12-debugging.md)
* [Interne Befehle](./13-intern.md)
