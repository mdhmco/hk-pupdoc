# Befehle für Variablen

* [SET-Befehl](#set-befehl)
* [EVAL-Befehl](#eval-befehl)
* [UNSET-Befehl](#unset-befehl)
* [LOCAL-Befehl](#local-befehl)
* [SETLOCAL-Befehl](#setlocal-befehl)
* [UNSETLOCAL-Befehl](#unsetlocal-befehl)


#### SET-Befehl

```
SET <var> <string>
```

* Weist der Variablen `<var>` den String `<string>` zu.
* Noch nicht existierende Variablen werden angelegt.
* Variablen, die über den SET-Befehl angelegt werden, haben globale Gültigkeit, d.h. ab dem Zeitpunkt der Anlage kann aus allen zeitlich nachfolgend ausgeführten Actions heraus auf diese zurückgegriffen werden.


{% codetabs name="Beispiel", type="puppet" -%}
ACTION eineAction
  # Definition einer Variablen
  SET derKugelschreiber "Kugelschreiber"
  SET dieZahlZehn 10
  # Änderung der Belegung einer Variablen
  SET derKugelschreiber "Auch ein anderer Inhalt geht"
  SET dieZahlZehn "Ups, nun ist's keine Zahl mehr :-)"
END
{%- endcodetabs %}



#### EVAL-Befehl

```
EVAL <var> = <ausdruck>
```

* Berechnet`<ausdruck>` (siehe Kapitel [Ausdrücke](../04-ausdruck/00-readme.md)) und weist das Ergebnis der Variablen `<var>` zu. 


{% codetabs name="Beispiel", type="puppet" -%}
ACTION eineAction
  # Definition einer Variablen
  SET counter 0
  # Hochzählen einer Variablen
  EVAL counter = [counter] + 1
END
{%- endcodetabs %}


- Variableninhalte sind Strings, sofern keine Typendeklaration oder -umwandlung stattgefunden hat. 
- Strings werden für arithmetische Operationen auch ohne explizites `INTEGER` davor zu einer Zahl gewandelt. Sodass damit gerechnet werden kann, wenn der Variableninhalt nur aus Ziffern besteht.


{% codetabs name="Beispiel", type="puppet" -%}
ACTION eineAction
  SET y 1
  >> /tell Kugelschreiber y = [y]
  EVAL y = [y] + 2
  >> /tell Kugelschreiber y = [y]
  EVAL y = Hans
  >> /tell Kugelschreiber y = [y]
  EVAL y = [y] + 2 # Berechnung kann nicht durchgeführt werden,
                   # da Inhalt "Hans" nicht numerisch
  >> /tell Kugelschreiber y = [y]
END

{%- language name="Zugehörige Ausgabe", type="txt" -%}
kuli997: y = 1
kuli997: y = 3
kuli997: y = Hans
kuli997: y = Hans
{%- endcodetabs %}

> Quelle: BSW Puppet Forum >> Fragen zu Strings - Antwort #3



#### UNSET-Befehl

```
UNSET <var>
```

* Löscht die Variable `<var>`, welche anschliessend nicht mehr definiert ist.


{% hint style='tip' %}
Im Falle der Existenz einer namensgleichen lokalen und globalen Variable hat der Befehl nur Auswirkung auf die lokale Variable.
{% endhint %}


{% codetabs name="Beispiel", type="puppet" -%}
ACTION eineAction
  SET a "aa"
  SET b "bb"
  SET c "cc"
  SET d "dd"
  SET e "ee"
  >> /tell Kugelschreiber 0.1: a=[a], b=[b], c=[c], d=[d], e=[e]
  DO zweiteAction
  >> /tell Kugelschreiber 0.2: a=[a], b=[b], c=[c], d=[d], e=[e]
END
# ----
ACTION zweiteAction
  >> /tell Kugelschreiber 1.1: a=[a], b=[b], c=[c], d=[d], e=[e]
  LOCAL a
  LOCAL b = "BB"
  SETLOCAL c "CC"
  LOCAL d = "DD"
  >> /tell Kugelschreiber 1.2: a=[a], b=[b], c=[c], d=[d], e=[e]
  UNSET b
  UNSETLOCAL c
  UNSET d
  UNSET e
  >> /tell Kugelschreiber 1.3: a=[a], b=[b], c=[c], d=[d], e=[e]
END

{%- language name="Zugehörige Ausgabe", type="txt" -%}
kuli999: 0.1: a=aa, b=bb, c=cc, d=dd, e=ee
kuli999: 1.1: a=aa, b=bb, c=cc, d=dd, e=ee
kuli999: 1.2: a=, b=BB, c=CC, d=DD, e=ee
kuli999: 1.3: a=, b=, c=, d=, e=
kuli999: 0.2: a=aa, b=bb, c=cc, d=dd, e=
{%- endcodetabs %}

> Quelle: BSW Puppet Forum >> Offene Fragen bzgl. Puppet-Dokumentation - Antwort #5



#### LOCAL-Befehl

```
LOCAL <var> [= <ausdruck>]
```

* Führt eine lokale Variable `<var>` ein.
* Der Begriff "lokal" bedeutet, dass diese Variable innerhalb des BEGIN/END-Blocks bzw. ACTION/END-Blocks existiert, in dem das `LOCAL` Kommando steht.
* Zusätzlich steht es in den Actions zur Verfügung, die aus dem definierenden Block heraus aufgerufen werden. 
* `LOCAL` dient der Definition (und erstmaligen Belegung durch einen Ausdruck) einer lokalen Variablen.
* Der Wert der Variablen kann anschliessend über `SET` oder `EVAL` verändert werden.

> Quelle: BSW Puppet Forum >> Daten aus SAVE-Variablen futsch - Antwort #16 und >> Puppet-Editor - Antwort #31


{% hint style='tip' %}
Existiert eine namensgleiche Variable (global oder lokal in einem darüber liegenden Block), so wird diese bis zum Rücksprung aus dem definierenden BEGIN/END-Block verdeckt (vgl. Beispiel II). Dies gilt auch nach Ausführung des Befehls `UNSET` oder `UNSETLOCAL` für die lokale Variable.
{% endhint %}


* Wird `<ausdruck>` angegeben, so wird dieser entsprechend dem `EVAL` Befehl ausgewertet und der neuen Variablen zugewiesen.
* *Einschränkungen bei der Wert-Übernahme*: Wird kein Ausdruck angegeben, so übernimmt die Variable nicht den Wert einer gleichlautenden globalen Variablen, wenn eine solche ausserhalb des definierenden Blocks existiert.
* *Einschränkungen bei der Wert-Weitergabe*: Der Wert einer lokalen Variablen wird nicht an eine namensgleiche globale Variable am Ende des BEGIN/END-Blocks weitergereicht. Der Inhalt wird einfach "vergessen".


{% codetabs name="Beispiel 1", type="puppet" -%}
ACTION eineAction
  # Definition globaler Variablen
  SET a "11"
  SET b "22"
  SET c "33"
  SET d "33"
  # Aufruf einer zweiten Action
  DO zweiteAction
  # Nach dem Aufruf existieren:
  #  a = "11" (Wert unverändert, da nur temp. überlagert)
  #  b = "22" (Wert unverändert, da nur temp. überlagert)
  #  c = "33" (Wert sowieso unverändert)
  #  d = "VierUndVierzig" (Wert verändert)
  #  e = "FünfUndFünfzig" (Variable hinzugefügt)
END
# ----
ACTION zweiteAction
  # Definition lokaler Variablen
  LOCAL a
  LOCAL b = "ZweiUndZwanzig"
  # Änderung/Definition globaler Variablen
  SET d "VierUndVierzig"
  SET e "FünfUndFünfzig"
  # Hier existieren folgende Variablen:
  #  a ist ohne Wert      (Local überlagtert Global)
  #  b = "ZweiUndZwanzig" (Local überlagt Global)
  #  c = "33"             (Global, Wert unverändert)
  #  d = "VierUndVierzig" (Global, Wert verändert)
  #  e = "FünfUndFünfzig" (Global, Variable hinzugefügt)
END

{%- language name="Beispiel 2", type="puppet" -%}
ACTION eineAction
  # Die lokale Variable "x" übernimmt den Inhalt der namensgleichen
  # globalen Variable "x" und verdeckt diese bis zum Rücksprung aus dem
  # definierenden BEGIN-END-Block
  LOCAL x = [x]
  EVAL x = [x] + 1 # Veränderung der lokalen Variablen "x"
  SET y = [x]      # Zuweisung des Inhalts der lokalen Variablen "x"
END
{%- endcodetabs %}



#### SETLOCAL-Befehl

```
SETLOCAL <var> <string>
```

* Dient der Definition (und erstmaligen Belegung durch einen String) einer lokalen Variablen.
* Der Wert der Variablen kann anschliessend über `SET` oder `EVAL` verändert werden.
* Alle übrigen Eigenschaften entsprechen denen des Befehls `LOCAL`.

{% hint style='tip' %}
Der Befehl `SETLOCAL` ist nicht gebräuchlich. Üblicherweise wird hierzu der Befehl `LOCAL` verwendet, welcher zusätzlich die Möglichkeit bietet, als Belegung einen Ausdruck zu verwenden.
{% endhint %}


{% codetabs name="Beispiel 1", type="puppet" -%}
ACTION eineAction
  SETLOCAL a [var]     # mit Befehl SETLOCAL
  # Anwendung LOCAL mit Initialisierung
  LOCAL b = [var]      # mit LOCAL. Identische Wirkung wie SETLOCAL.
  LOCAL c = [var] + 1  # Zusätzliche Möglichkeit von LOCAL
  # Anwendung LOCAL ohne Initialisierung
  LOCAL b
  SET b [var]
  LOCAL c
  EVAL c = [var] + 1
END
{%- endcodetabs %}

> Quelle: BSW Puppet Forum >> Offene Fragen bzgl. Puppet-Dokumentation - Antwort #1 und >> Puppet-Editor - Antwort #31



#### UNSETLOCAL-Befehl

```
UNSETLOCAL <var>
```

* Dies ist der `UNSET`-Befehl für eine lokale Variable.
* Eine lokale Variable kann aber auch mit dem normalen `UNSET` gelöscht werden.
* Letzteres ist deshalb auch bei lokalen Variablen der gebräuchlichere Befehl.


{% hint style='tip' %}
Oft kann auf das `UNSET` bei einer lokalen Variable verzichtet werden, da mit Beendigung der Action / des Blocks, in der/dem die lokale Variable definiert worden ist, die lokale Variable automatisch gelöscht wird. 
{% endhint %}

> Quelle: BSW Puppet Forum >> Offene Fragen bzgl. Puppet-Dokumentation - Antwort #1 und #5 und >> Programmierstil vs. Performence? - Antwort #1
