# Actions

```
ACTION <name>
  <befehle>
END
```

Durch diverse Events bzw. Befehle werden Actions aufgerufen. Dies bedeutet, dass die Befehle der jeweiligen Action der Reihe nach ausgeführt werden.


## Action "start"

Das Puppet beginnt direkt nach dem Laden damit, die Action "start" auszuführen, sofern diese existiert.


{% hint style='tip' %}
Die Action "start" kann weggelassen werden. Das ist sinnvoll, falls das Puppet komplett über @-Befehle gesteuert wird.
{% endhint %}
