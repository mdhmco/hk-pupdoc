# DISAPPEAR-Ereignis

* [WHEN DISAPPEAR](#when-disappear)
* [IGNORE DISAPPEAR](#ignore-disappear)
* [IGNOREALL DISAPPEAR](#ignoreall-vappear)


#### WHEN DISAPPEAR

```
WHEN DISAPPEAR <liste> DO <action>
```

- Wenn jemand aus `<liste>` den Raum verlässt wird die Action `<action>` ausgeführt.
- Zusätzlich geschieht dies durch die Eigenbewegung des Puppets. 


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Person, die den Raum verlässt
{%- endcodetabs %}


{% hint style='tip' %}
Personen, deren Name mit "Geist" beginnt, werden ignoriert (=Leute, die connected haben, aber noch nicht eingeloggt sind).
{% endhint %}



#### IGNORE DISAPPEAR

```
IGNORE DISAPPEAR <liste>
```

- In Zukunft wird nichts gemacht, wenn jemand aus `<liste>` den Raum verlässt.
- Genauer ausgedrückt: Es wird ein vorhergehendes `WHEN DISAPPEAR <liste> DO xxx`  gelöscht.



#### IGNOREALL DISAPPEAR

```
IGNOREALL DISAPPEAR
```

- Alle DISAPPEAR-Einträge werden gelöscht (default).
