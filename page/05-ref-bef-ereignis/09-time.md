# TIME-Ereignis

* [WHEN TIME](#when-time)
* [IGNORE TIME](#ignore-time)


#### WHEN TIME

```
WHEN TIME <hour>[:<min>] DO <action>
```

- Jedes Mal zu der (Server-)Uhrzeit, die von `<hour>` und `<min>` bestimmt wird, wird die Action `<action>` ausgeführt. 


{% hint style='warning' %}
Das Puppet kann sich nur eine Uhrzeit merken.
{% endhint %}



#### IGNORE TIME

```
IGNORE TIME
```

- Puppet führt nichts mehr zu einer bestimmten Uhrzeit aus.
