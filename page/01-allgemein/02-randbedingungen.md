# Randbedingungen

Folgende Randbedingungen gelten für das Dokument:

- Das Dokument wird aktuell vom BSW-User "Kugelschreiber" erstellt.
- Im Dokument sind die Inhalte aus der offiziellen BSW Puppet-Hilfe sowie die sporadischen Informationen innerhalb des BSW Puppet-Forums enthalten.
- Ferner handelt es sich um vereinzelte Erweiterungen aus 2022, die per PN mitgeteilt wurden.

Das Dokument wurde unter [GitLab](https://gitlab.com/) in zwei Ausprägungen zur Verfügung gestellt.


#### Basis der Generierung der Dokumentation

Dies ist ein weiterer Anlauf, die Dokumentation in einer Form bereitzustellen, die mit übersichtlichem Aufwand erzeugt werden kann. Dies gilt insbesondere in dem Punkt der Software, die hierfür zu installieren ist.

Mittlerweilen basiert diese Dokumentation auf [HonKit](https://github.com/honkit/honkit). Die beiden vorangegangenen Versionen:
- Auf Basis von [VuePress](https://vuepress.vuejs.org/); eine weitere Überarbeitung ist faglich.
- Auf Basis von [Sphinx](https://www.sphinx-doc.org/en/master/); diese wird ebenfalls **nicht** mehr fortgeführt.
