# Grundlagen

Hier sind die Grundlagen eines Puppets angeführt:

* [Aufbau eines Puppets](./02-aufbau.md)
* [Puppeteinstellungen](./03-einstellungen.md)
* [@-Befehle](./04-addbefehle.md)
* [Sprachelemente](./05-sprachelemente.md)
* [Actions](./06-actions.md)
