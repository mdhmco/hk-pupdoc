# Allgemeines zum Dokument

Die Motivation zur Erstellung dieses Dokuments war im Februar 2006 das Bestreben, auf eine aktuelle Befehlsliste der Puppet-Programmierung in der [Brettspielwelt](http://www.brettspielwelt.de) (=BSW) Zugriff zu haben. Seitdem sind viele Jahre vergangen und die Häufigkeit der Aktualisierung hat kontinuierlich nachgelassen.

Die Bedeutung von Puppets in der BSW ist seit Einführung der mobilen Version und damit der "Apps" grundsätzlich zurückgegangen. Eine Abkehr von dieser Tendenz ist seit (geschätzt) 2020 erkennbar.


{% hint style='warning' %}
Eindringlicher Hinweis: Es handelt sich um **KEIN** offizielles Dokument der BSW!
{% endhint %}


## Neuerungen in der Puppetsprache

Neuerungen gab es eine lange Zeit nicht (mehr). Erst im Laufe des Jahres 2022 wurden wieder ein paar Kleinigkeiten hinzugefügt.


<!-- <p class="box note">
  Es handelt sich um *KEIN* offizielles Dokument der BSW!
</p> -->
