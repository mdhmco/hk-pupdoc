# PING-Ereignis

* [WHEN PING](#when-ping)
* [IGNORE PING](#ignore-ping)


#### WHEN PING

```
WHEN PING [FROM <liste>] DO <action>
```

- Wenn jemand aus `<liste>` das Puppet anpingt, wird die Action  `<action>` ausgeführt.


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Person (bzw. Puppet), die den Ping ausgelöst hat
{%- endcodetabs %}



#### IGNORE PING

```
IGNORE PING [FROM <liste>]
```

- Wenn jemand aus `<liste>` das Puppet anpingt, wird nichts mehr gemacht.
- Wird `<liste>` ganz weggelassen, reagiert das Puppet auf überhaupt keine Pings mehr.
