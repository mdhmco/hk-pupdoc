# Befehle für Debugging

* [DEBUG-Befehl](#debug-befehl)
* [VARDUMP-Befehl](#vardump-befehl)
* [EVENTDUMP-Befehl](#eventdump-befehl)
* [QUEUEDUMP-Befehl](#queuedump-befehl)


#### DEBUG-Befehl

```
DEBUG ON
DEBUG OFF
```

* Schaltet den [Debugging-Modus](../10-other/02-debug/02-dbgmodus.md) für einzelne Bereichs des Puppets an bzw. aus.
* Dies dient der Einschränkung der anfallenden Datenmengen.



#### VARDUMP-Befehl

```
VARDUMP
```

* Gibt alle Variablen und deren Belegung aus (`VD`).
* Der Befehl kann nur im aktivierten [Debugging-Modus](../10-other/02-debug/02-dbgmodus.md) benutzt werden.


{% codetabs name="Beispiel", type="puppet" -%}
@vd: vardump
# ...
ACTION vardump
  DEBUG ON
  VARDUMP
  DEBUG OFF
END
{%- endcodetabs %}



#### EVENTDUMP-Befehl

```
EVENTDUMP
```

* Gibt alle Events auf die das Puppet reagiert (WHEN-Befehle) aus (`ED..`). 
* Der Befehl kann nur im aktivierten [Debugging-Modus](../10-other/02-debug/02-dbgmodus.md) benutzt werden.


{% codetabs name="Beispiel", type="puppet" -%}
@ed: eventdump
# ...
ACTION eventdump
  DEBUG ON
  EVENTDUMP
  DEBUG OFF
END
{%- endcodetabs %}


{% codetabs name="Format der Ausgaben", type="puppet" -%}
EDCH <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
EDKW <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
EDMA <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
EDCL <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
EDTR <action> [<interval>]
EDKK <action>
EDO <action> [FROM <who>]
EDP <action> [FROM <who>]
EDE <action>
{%- endcodetabs %}

* Kennnung =
  - `ED..` = EventDump
  - `..CH` = Event des Typs `CHAT`
  - `..KW` = Event des Typs `KEYWORD`
  - `..MA` = Event des Typs `MATCH`
  - `..CL` = Event des Typs `CLICKED`
  - `..TR` = Event des Typs `TIMER`
  - `..KK` = Event des Typs `KICKED`
  - `..O` = Event des Typs `OWNED`
  - `..P` = Event des Typs `PING`
  - `..E` = Event des Typs `ERROR`

* `<text>` = Text/Keyword/Match für den ``CHAT`` des Events
* `<who>` = Einschränkung auf `WHO` (#, wenn nicht spezifiziert)
* `<type>` = Einschränkung auf `TYPE` (, wenn nicht spezifiziert)
* `<room>` = Einschränkung auf `ROOM`, also Raum/Kanal (#, wenn nicht spezifiziert)
* `<intervall>` = Länge des Intervalls
* `<action>` = Name der Action



#### QUEUEDUMP-Befehl

```
QUEUEDUMP
```

* Gibt alle Befehle aus, die darauf warten ("Warteschlange"), vom Puppet ausgeführt zu werden (`QD`). 
* Der Befehl kann nur im aktivierten [Debugging-Modus](../10-other/02-debug/02-dbgmodus.md) benutzt werden.


{% codetabs name="Beispiel", type="puppet" -%}
@qd: queuedump
# ...
ACTION queuedump
  DEBUG ON
  QUEUEDUMP
  DEBUG OFF
END
{%- endcodetabs %}
