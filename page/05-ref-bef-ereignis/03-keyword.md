# KEYWORD-Ereignis

* [WHEN KEYWORD](#when-keyword)
* [IGNORE KEYWORD](#ignore-keyword)
* [IGNOREALL KEYWORD](#ignoreall-keyword)


#### WHEN KEYWORD

```
WHEN KEYWORD <keywordliste> [erweiterte WHEN Bedingungen] DO <action>
```

- Wenn das Puppet Text mitbekommt, welcher eines der Wörter aus `<keywordliste>` enthält, so wird die Action `<action>` ausgeführt.
- Das Wort muss dabei durch Leerzeichen vom Rest der Zeile abgegrenzt sein.
- Kommen mehrere (auch gleiche) KEYWORDs vor, so wird für jedes KEYWORD die zugehörige Action `<action>` (auch mehrmals) ausgeführt, und zwar von links  nach rechts.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden..


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Sprecher des Textes
`CHAT` = Eingegebener Text
`TYPE` = Art des Chats
`ROOM` = Aufenthaltsort des Sprechers
`ROOMNAME` = Angabe zum Aufenthaltsort des Sprechers
{%- endcodetabs %}


> Siehe: `TYPE`, `ROOM`und `ROOMNAME` im Kapitel [Variablen von Befehlen](../03-referenz/02-1-var_befehle.md)


{% codetabs name="Beispiel", type="puppet" -%}
ACTION start
  WHEN KEYWORD "!info" TYPE "TELL" DO info
  # Wenn zu zwei Quellen die selbe ACTION registriert werden soll,
  # kann man diese wie folgt zusammenfassen:
  WHEN KEYWORD "!hilfe" TYPE "GTELL TELL" DO hilfe
END
{%- endcodetabs %}


{% hint style='tip' %}
Um die versehentliche Ausführung von Kommandos durch beliebige Texteingabe zu verhindern, wird dem Keyword oft ein Sonderzeichen vorangestellt. Beispiel: `!doit`, `$doit`, ....
{% endhint %}



#### IGNORE KEYWORD

```
IGNORE KEYWORD <liste> [erweiterte WHEN Bedingungen]
```

- Die Schlüsselwörter aus `<keywordliste>` werden ab sofort ignoriert bzw. nicht mehr gesondert behandelt, falls zu Schlüsselwort(en) noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.



#### IGNOREALL KEYWORD

```
IGNOREALL KEYWORD [erweiterte WHEN Bedingungen]
```

- Das Puppet reagiert nicht mehr, bzw. nicht mehr gesondert auf beliebige  Schlüsselwörter.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed.md#erweiterte_when_bedingungen) eingeschränkt werden.
