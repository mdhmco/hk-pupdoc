# Ausdrücke für Variablen

* [EXISTS](#exists)


#### EXISTS

```
EXISTS <var>
```

Liefert `TRUE`, falls die Variable mit dem Namen `<var>` einen Wert enthält, ansonsten `FALSE`.


```
EXISTS <liste>
```

Für Listen erweitertes EXISTS.


{% codetabs name="Beispiel", type="puppet" -%}
SET a "X"
SET b "X"
SET x "X"
>> /tell Kugelschreiber EXISTS "a" >> [=EXISTS "a"]
>> /tell Kugelschreiber EXISTS "a b c" >> [=EXISTS "a b c"]
>> /tell Kugelschreiber EXISTS "a b x" >> [=EXISTS "a b x"]

{%- language name="Zugehörige Ausgabe", type="txt" -%}
kulipuptest: EXISTS "a" >> TRUE
kulipuptest: EXISTS "a b c" >> TRUE TRUE FALSE
kulipuptest: EXISTS "a b x" >> TRUE TRUE TRUE
{%- endcodetabs %}
