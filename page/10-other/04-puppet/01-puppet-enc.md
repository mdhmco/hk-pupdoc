# Puppet Zeichensatz

(Zitat SLC:) There are now several possibilities to use multibyte character source code:

* A line in the sourcecode (best directly after "`PUPPET xyz`") like:

  - `CHARSET: "ISO-8859-1"`
  - `CHARSET: "UTF-8"`
  - `CHARSET: "Shift_JIS"`

* A modification of the registration:

  - `/puppetdef set PUPPETNAME charset="ISO-8859-1"`
  - `/puppetdef set PUPPETNAME charset="UTF-8"`
  - `/puppetdef set PUPPETNAME charset="Shift_JIS"`

* A given URL that ends with "\_UTF-8" (UTF-8 only)

  - `/puppetdef set PUPPETNAME url="http://my.computer/puppet.prog_UTF-8"`
  - `/puppet start PUPPETNAME`
  - `/puppet CommandPuppet PUPPETNAME http://my.computer/puppet.prog_UTF-8`

* Certain UTF-8 markers (UTF-8 only, unsafe)

  - `/puppetdef set PUPPETNAME url="http://my.computer/puppet.prog_UTF-8"`
  - `/puppet start PUPPETNAME`
  - `/puppet CommandPuppet PUPPETNAME http://my.computer/puppet.prog_UTF-8`


{% codetabs name="Beispiel", type="puppet" -%}
/puppet TP CantStop-ja
/puppet TP ParisParis-zh-tr
{%- endcodetabs %}

> Quelle: BSW Puppet Forum >> How to use a codepage/charset other than ISO-8859-1 (latin1) => UTF-8, Shift_JIS

