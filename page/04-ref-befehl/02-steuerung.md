# Befehle zur Steuerung

* [SLEEP-Befehl](#sleep-befehl)
* [MASTERRESET-Befehl](#masterreset-befehl)
* [HARAKIRI-Befehl](#harakiri-befehl)


#### SLEEP-Befehl

```
SLEEP <sek> [<millisek>]
```

- Verzögert die Ausführung um `<sek>` Sekunden und `<millisek>` Millisekunden.


{% codetabs name="Beispiel", type="puppet" -%}
ACTION eineAction
  # Verzögerung von 2 Sekunden
  SLEEP 2
END
{%- endcodetabs %}


{% hint style='tip' %}
Diesen Befehl sollte man von Zeit zu Zeit ausführen, damit das Puppet den Server nicht zu sehr belastet. Während der Verzögerungszeit reagiert das Puppet auf nichts. - Auftretende Events werden danach abgearbeitet.
{% endhint %}



#### MASTERRESET-Befehl

```
MASTERRESET
```

- Führt einen Neustart des Puppets durch. Alle Variablen und alle `WHEN`-Einstellungen, sowie alle wartenden Events werden gelöscht.



#### HARAKIRI-Befehl

```
HARAKIRI [<text>]
```

- Das Puppet beendet sich selbst.
- Der Parameter wird im Chat in der Form `-- [PuppetConsole] HARAKIRI: <text>` ausgegeben.
