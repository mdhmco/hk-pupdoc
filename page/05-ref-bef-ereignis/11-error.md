# ERROR-Ereignis

* [WHEN ERROR](#when-error)
* [IGNORE ERROR](#ignore-error)


#### WHEN ERROR

```
WHEN ERROR DO <action>
```

- Tritt ein Fehler bei der Ausführung eines Puppets auf (Teilen durch 0, etc.)  so wird die Action `<action>` ausgeführt (und zwar sofort und nicht erst nachdem die aktuelle Action beendet wurde). 


{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`MESSAGE` = Fehlermeldung (in der Regel eine Java-Fehlermeldung)
{%- endcodetabs %}


{% hint style='warning' %}
Tritt während der Ausführung der Action `<action>` ein Fehler auf, so wird diese nicht erneut ausgeführt. 
{% endhint %}



#### IGNORE ERROR

```
IGNORE ERROR
```

- Tritt ein Fehler auf, so wird nichts gemacht.
