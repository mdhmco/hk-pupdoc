# Befehle für Ereignisse

Erläuterung der Befehle für Ereignisse.

* [Infos zu Befehlen für Ereignisse](./01-info-erwbed.md)
* [CHAT-Ereignis](./02-chat.md)
* [KEYWORD-Ereignis](./03-keyword.md)
* [MATCH-Ereignis](./04-match.md)
* [CLICKED-Ereignis](./05-clicked.md)
* [NEWROOM-Ereignis](./06-newroom.md)
* [APPEAR-Ereignis](./07-appear.md)
* [DISAPPEAR-Ereignis](./08-disappear.md)
* [TIME-Ereignis](./09-time.md)
* [TIMER-Ereignis](./10-timer.md)
* [ERROR-Ereignis](./11-error.md)
* [KICKED-Ereignis](./12-kicked.md)
* [PING-Ereignis](./13-ping.md)
* [OWNED-Ereignis](./14-owned.md)
* [UNOWNED-Ereignis](./15-unowned.md)
