# Referenz Ausdrücke

Erläuterung der Ausdrücke.

* [Ausdrücke für Variablen](./01-variablen.md)
* [Ausdrücke für Boolsche Werte](./02-boolsche.md)
* [Ausdrücke für Zahlen](./03-zahlen.md)
* [Ausdrücke für Strings](./04-strings.md)
* [Ausdrücke für Listen](./05-listen.md)
* [Sonstige Ausdrücke](./06-sonstig.md)
* [Ausdrücke für Typprüfung](./07-typpruefung.md)
* [Ausdrücke für Typumwandlung](./08-typumwand.md)
* [Auswertung](./09-auswertung.md)


#### Besonderer Hinweis

{% hint style='warning' %}
Bei Ausdrücken müssen alle Symbole / Variablen / Konstanten etc. durch Leerzeichen voneinander abgetrennt werden.
{% endhint %}
