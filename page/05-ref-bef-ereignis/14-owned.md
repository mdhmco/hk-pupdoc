# OWNED-Ereignis

* [WHEN OWNED](#when-owned)
* [IGNORE OWNED](#ignore-owned)


#### WHEN OWNED

```
WHEN OWNED [FROM <liste>] DO <action>
```

- Wenn jemand von `<liste>` das Puppet owned, wird die Action `<action>` ausgeführt.
  

{% codetabs name="Liste der belegten Variablen", type="puppet" -%}
`WHO` = Person, die das Puppet owned
{%- endcodetabs %}


> Siehe: Kapitel [Einstellungen für Puppetowning &raquo; NEWOWNER](../02-grundlagen/03-einstellungen.md#einstellungen-fur-puppetowning)



#### IGNORE OWNED

```
IGNORE OWNED [FROM <liste>]
```

- Die Leute von `<liste>` werden nicht mehr beim ownen beachtet.
- Fehlt die Liste ganz, so wird überhaupt nicht mehr auf ownen geachtet.
